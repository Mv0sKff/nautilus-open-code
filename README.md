# OpenInCode

<p>Simple script to VSCodium from Nautilus (Gnome Files) Menu</p>


<p align="center">
  <img src="./preview.png" />
</p>


## Dependency
`nautilus-python`( `python-nautilus` on Debian/Ubuntu based)
### Ubuntu
```
sudo apt install python3-nautilus
```
### Fedora
```
sudo dnf install nautilus-python
```

## Installation

Clone this repository and use the install script.
```
git clone https://gitlab.com/Mv0sKff/nautilus-open-code.git
cd nautilus-open-code
./install.sh
```
or install system-wide with sudo
```
sudo ./install.sh
```

Fork of [https://github.com/ppvan/nautilus-open-in-blackbox](https://github.com/ppvan/nautilus-open-in-blackbox)